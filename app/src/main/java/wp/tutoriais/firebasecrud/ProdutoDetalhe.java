package wp.tutoriais.firebasecrud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class ProdutoDetalhe extends AppCompatActivity {
    //Iniciando a conexão com o Firebase Firestore
    FirebaseFirestore conexaoBD = FirebaseFirestore.getInstance();
    //Objeto da classe AlertDialog para confirmação da remoção
    AlertDialog alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produto_detalhe);

        //Recuperando o valor do código do produto que foi passado pela Intent
        int codigoProduto = getIntent().getIntExtra("codigoProduto", -1);

        //Criando objeto para cada componente da tela
        TextView tvCodigo = findViewById(R.id.tvCodigo);
        TextView tvProduto = findViewById(R.id.tvProduto);
        TextView tvCategoria = findViewById(R.id.tvCategoria);
        TextView tvAtivo = findViewById(R.id.tvAtivo);
        TextView tvPreco = findViewById(R.id.tvPreco);
        Button btAPagar = findViewById(R.id.btApagar);
        Button btVoltar = findViewById(R.id.btVoltar);

        //Iniciando a consulta na coleção "produtos" e buscando o produto através do seu código
        conexaoBD.collection("produtos")
                .document(codigoProduto + "") //Indicando o código do documento
                .get() //Requisitando os dados
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        //Se o comando for executado com sucesso, então
                        if (task.isSuccessful()) {
                            //Recuperando o documento (caso encontrado)
                            DocumentSnapshot document = task.getResult();

                            //Testando se o documento contém dados válidos
                            if (document.exists()) {
                                //Convertendo o documento para um objeto do tipo "Produto"
                                Produto p = document.toObject(Produto.class);

                                //Atribuindo os valores do "Produto" para os campos da tela
                                tvCodigo.setText("Código: " + p.getCodigo());
                                tvProduto.setText("Nome: " + p.getNome());
                                tvCategoria.setText("Categoria: " + p.getCategoria());
                                tvAtivo.setText("Ativo? (s/n): " + (p.isAtivo() ? "Sim" : "Não"));
                                tvPreco.setText("R$ " + p.getPreco());

                            } else {
                                //Exibindo informação caso apresente um erro na conexão
                                Snackbar.make(findViewById(R.id.tela), R.string.erroBusca, Snackbar.LENGTH_SHORT).show();
                            }
                        } else {
                            //Exibindo informação caso apresente um erro na conexão
                            Snackbar.make(findViewById(R.id.tela), R.string.erroBusca, Snackbar.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Caso a busca encontre algum erro, esse método é chamado
                        Snackbar.make(findViewById(R.id.tela), R.string.erroBusca, Snackbar.LENGTH_SHORT).show();
                    }
                });

        //Evento do botão "Voltar"
        btVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish(); //Encerra a tela atual
            }
        });

        //Evento do botão "Apagar"
        btAPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ProdutoDetalhe.this);
                alert.setTitle("Apagar produto");
                alert.setMessage("Deseja mesmo remover o produto?");

                //define um botão como positivo
                alert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        //Caso a pessoa clicou para confirmar a remoção, então
                        //Iniciando a consulta na coleção "produtos" e buscando o produto através do seu código
                        conexaoBD.collection("produtos")
                                .document(codigoProduto + "") //Indicando o código do documento
                                .delete()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()) {
                                            //Caso a remoção ocorra corretamente
                                            Snackbar.make(findViewById(R.id.tela), R.string.sucessoApaga, Snackbar.LENGTH_SHORT).show();
                                        }else{
                                            //Caso a remoção encontre algum erro, esse método é chamado
                                            Snackbar.make(findViewById(R.id.tela), R.string.erroBusca, Snackbar.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        //Caso a remoção encontre algum erro, esse método é chamado
                                        Snackbar.make(findViewById(R.id.tela), R.string.erroBusca, Snackbar.LENGTH_SHORT).show();
                                    }
                                });
                    }
                });
                //define um botão como negativo
                alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        alerta.cancel();
                    }
                });

                //Criar um objeto da classe AlertDialog
                alerta = alert.create();
                //Exibir
                alerta.show();
            }
        });

    }
}