package wp.tutoriais.firebasecrud;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProdutosViewHolder extends RecyclerView.ViewHolder {
    TextView produtoItem, categoriaItem, precoItem;
    ImageButton visualizarItem;

    public ProdutosViewHolder(@NonNull View itemView) {
        super(itemView);
        //Vincular o ID para cada componente
        produtoItem = itemView.findViewById(R.id.tvProdutoItem);
        categoriaItem = itemView.findViewById(R.id.tvCategoriaItem);
        precoItem = itemView.findViewById(R.id.tvPrecoItem);
        visualizarItem = itemView.findViewById(R.id.ibVisualizarItem);
    }
}
