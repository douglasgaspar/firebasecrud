package wp.tutoriais.firebasecrud;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProdutoAdapter extends RecyclerView.Adapter<ProdutosViewHolder> {
    List<Produto> listaProdutos; //Lista da classe para ser usada em mais de um método

    //Construtor
    public ProdutoAdapter(List<Produto> listaProdutos){
        this.listaProdutos = listaProdutos;
    }

    @NonNull
    @Override
    public ProdutosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Chamando o layout_item.xml para definir como modelo a ser usado
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item, parent, false);
        return new ProdutosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProdutosViewHolder holder, int position) {
        //Convertendo o objeto viewHolder para o nosso ViewHolder
        ProdutosViewHolder produtoVH  = (ProdutosViewHolder) holder;

        //Agora podemos acessar os nossos componentes através do objeto "produtoVH"
        //para atribuir os valores de cada campo
        produtoVH.produtoItem.setText(listaProdutos.get(position).getNome());
        produtoVH.categoriaItem.setText(listaProdutos.get(position).getCategoria());
        produtoVH.precoItem.setText("R$: " + listaProdutos.get(position).getPreco());

        //Referência do produto que foi clicado pelo usuário
        Produto produtoClique = listaProdutos.get(position);

        //Evento a ser acionado quando for clicado na imagem da lupa sobre o arquivo
        produtoVH.visualizarItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Indicando qual a tela que será aberta, no caso a "ProdutoDetalhe"
                Intent it = new Intent(produtoVH.itemView.getContext(), ProdutoDetalhe.class);
                //Adicionando um valor para ser passado como informação para a tela a ser aberta
                it.putExtra("codigoProduto", produtoClique.getCodigo());
                //Requisitando que a tela "ProdutoDetalhe" seja aberta
                produtoVH.itemView.getContext().startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaProdutos.size();
    }
}
