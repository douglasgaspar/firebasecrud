package wp.tutoriais.firebasecrud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity {
    //Iniciando a conexão com o Firebase Firestore
    FirebaseFirestore conexaoBD = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Criação de objeto para os componentes da tela
        EditText edCodigo = findViewById(R.id.edCodigo);
        EditText edNome = findViewById(R.id.edNome);
        EditText edCategoria = findViewById(R.id.edCategoria);
        EditText edPreco = findViewById(R.id.edPreco);
        Switch swAtivo = findViewById(R.id.swAtivo);
        Button btCadastrar = findViewById(R.id.btCadastrar);
        Button btListar = findViewById(R.id.btListar);

        btListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Criação da Intent para indicar a intenção de abrir a tela "ListaProdutos"
                Intent it = new Intent(MainActivity.this, ListaProdutos.class);
                //Comando para abrir a tela indicada no comando acima
                startActivity(it);
            }
        });

        //Adicionando o evento de clique no botão Cadastrar
        btCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Verificação se os campos de código, nome e preço estão preenchidos
                if(edCodigo.getText().toString().isEmpty() || edNome.getText().toString().isEmpty() || edPreco.getText().toString().isEmpty()){
                    //Se algum dos campos acima estiver sem preenchimento, será exibida uma mensagem ao usuário
                    Snackbar.make(findViewById(R.id.tela), R.string.camposCadastro, Snackbar.LENGTH_LONG).show();
                }else{
                    //Caso ao menos os campos acima estiverem preenchidos...
                    //Cria-se um objeto da classe Produto com os valores preenchidos
                    Produto p = new Produto(Integer.parseInt(edCodigo.getText().toString()),
                            edNome.getText().toString(),
                            edCategoria.getText().toString(),
                            Double.parseDouble(edPreco.getText().toString()),
                            swAtivo.isChecked());

                    //Com o objeto preenchido, podemos requisitar o envio ao Firebase Firestore
                    //A inserção de dados é realizada dentro de uma coleção, é necessário atribuir um nome para a coleção
                    conexaoBD.collection("produtos")
                        .document(edCodigo.getText().toString()) //Especificando o ID do documento
                        .set(p) //O objeto que será cadastrado no ID acima
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                //Caso o cadastro aconteça sem erro, esse método é chamado
                                if(task.isSuccessful()){ //Verificação se a inserção foi completada
                                    Snackbar.make(findViewById(R.id.tela), R.string.sucessoCadastro, Snackbar.LENGTH_SHORT).show();
                                }else{
                                    Snackbar.make(findViewById(R.id.tela), R.string.erroCadastro, Snackbar.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                //Caso o cadastro encontre algum erro, esse método é chamado
                                Snackbar.make(findViewById(R.id.tela), R.string.erroCadastro, Snackbar.LENGTH_SHORT).show();
                                e.printStackTrace(); //Imprime a pilha de erros
                            }
                        });
                }
            }
        });

    }
}